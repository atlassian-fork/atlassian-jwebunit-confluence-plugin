package com.atlassian.confluence.junit3;

class TestTimer {
    private long setUpstart, testStart, tearDownStart, end;

    public static TestTimer start() {
        TestTimer testTimer = new TestTimer();
        testTimer.setUpstart = System.currentTimeMillis();
        return testTimer;
    }

    private TestTimer() {

    }

    public void endSetUp() {
        testStart = System.currentTimeMillis();
    }

    public void endTest() {
        tearDownStart = System.currentTimeMillis();
    }

    public void endTearDown() {
        end = System.currentTimeMillis();
    }

    public void log(String testName, boolean testPassed) {
        double setUpSecs = diff(setUpstart, testStart);
        double testSecs = diff(testStart, tearDownStart);
        double tearDownSecs = diff(tearDownStart, end);
        double totalSecs = diff(setUpstart, end);
        String status = testPassed ? "   " : " X ";

        String times = String.format("%s Test ran in %4.1fs (%4.1f + %4.1f + %4.1f): %s", status, totalSecs, setUpSecs, testSecs,
                tearDownSecs, testName);
        System.out.println(times);
    }

    private double diff(long start, long end) {
        return (end - start) / 1000.0;
    }
}